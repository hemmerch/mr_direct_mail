<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2013 Eric Chavaillaz (eric@hemmer.ch)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *  A copy is found in the textfile GPL.txt and important notices to the license
 *  from the author is found in LICENSE.txt distributed with these scripts.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * Class Tx_MrDirectMail_UserFunction_TtAddressGroupList implements a user function to send the newsletter to a selection of "tt_address_group" records.
 *
 * @author Eric Chavaillaz <eric@hemmer.ch>
 */
class Tx_MrDirectMail_UserFunction_TtAddressGroupList {
	/**
	 * Adds all addresses from the given "tt_address_group" or/and given condition to the recipients list.
	 *
	 * @param array $parameters
	 * @param ux_tx_directmail_recipient_list|ux_tx_directmail_dmail $parentObject
	 * @return void
	 */
	public function ttAddressGroupList(array &$parameters, $parentObject) {
		$ttAddressGroups = t3lib_div::intExplode(',', $parameters['userParams']['tt_address_groups'], TRUE);
		$ttAddressGroups = implode(',', $ttAddressGroups);

		if (!empty($ttAddressGroups)) {
			$parameters['lists']['tt_address'] = $this->getAddressesInGroup($parameters, $ttAddressGroups);
		} else {
			$parameters['lists']['tt_address'] = $this->getAllAddresses($parameters);
		}
	}

	/**
	 * Returns all the addresses from the given "tt_address_group" who match the additional where clause.
	 *
	 * @param array $parameters
	 * @param string $ttAddressGroups
	 * @return mixed
	 */
	protected function getAddressesInGroup(array &$parameters, $ttAddressGroups) {
		$resource = $GLOBALS['TYPO3_DB']->exec_SELECT_mm_query(
			'tt_address.uid',
			'tt_address',
			'tt_address_group_mm',
			'tt_address_group',
			' AND tt_address.hidden = 0 ' . t3lib_BEfunc::deleteClause('tt_address') . ' AND tt_address_group.uid IN (' . $ttAddressGroups . ')' . $this->getAdditionalWhereClause($parameters['userParams'])
		);

		while (($actionRow = $GLOBALS['TYPO3_DB']->sql_fetch_assoc($resource)) !== FALSE) {
			$parameters['lists']['tt_address'][] = $actionRow['uid'];
		}
		$GLOBALS['TYPO3_DB']->sql_free_result($resource);

		return array_unique($parameters['lists']['tt_address']);
	}

	/**
	 * Returns all the addresses who match the additional where clause.
	 *
	 * @param array $parameters
	 * @return mixed
	 */
	protected function getAllAddresses(array &$parameters) {
		$rows = $GLOBALS['TYPO3_DB']->exec_SELECTgetRows(
			'tt_address.uid',
			'tt_address',
			'tt_address.hidden = 0 ' . t3lib_BEfunc::deleteClause('tt_address') . $this->getAdditionalWhereClause($parameters['userParams'])
		);

		foreach ($rows as $row) {
			$parameters['lists']['tt_address'][] = $row['uid'];
		}

		return array_unique($parameters['lists']['tt_address']);
	}

	/**
	 * Gets the additional where clauses.
	 *
	 * @param array $parameters
	 * @return string
	 */
	protected function getAdditionalWhereClause(array $parameters) {
		$additionalWhere = '';

		if (!empty($parameters['tt_address_gender'])) {
			$additionalWhere = ' AND tt_address.gender = \'' . $parameters['tt_address_gender'] . '\'';
		}

		return $additionalWhere;
	}

	/**
	 * Returns an array of the field definitions for the additional parameters.
	 *
	 * @param string $methodName
	 * @return array
	 */
	public function getWizardFields($methodName) {
		return array(
			'columns' => array(
				'tt_address_groups' => array(
					'label' => 'LLL:EXT:mr_direct_mail/Resources/Private/Language/locallang_db.xml:fields.tt_address_groups',
					'config' => array(
						'type' => 'select',
						'foreign_table' => 'tt_address_group',
						'foreign_table_where' => ' AND tt_address_group.hidden = 0 ' . t3lib_BEfunc::deleteClause('tt_address_group') . ' ORDER BY tt_address_group.title',
						'size' => 10,
						'maxitems' => 50,
						'wizards' => array(
							'_PADDING' => 1,
							'_VERTICAL' => 1,
							'add' => array(
								'type' => 'script',
								'title' => 'LLL:EXT:mr_direct_mail/Resources/Private/Language/locallang_db.xml:wizards.new_item',
								'icon' => 'add.gif',
								'script' => 'wizard_add.php',
								'params' => array(
									'table' => 'tt_address_group',
									'pid' => '###CURRENT_PID###',
									'setValue' => 'prepend'
								)
							)
						)
					)
				),
				'tt_address_gender' => array(
					'label' => 'LLL:EXT:mr_direct_mail/Resources/Private/Language/locallang_db.xml:fields.tt_address_gender',
					'config' => array(
						'type' => 'select',
						'items' => array(
							array('LLL:EXT:mr_direct_mail/Resources/Private/Language/locallang_db.xml:fields.tt_address_gender.default', ''),
							array('LLL:EXT:mr_direct_mail/Resources/Private/Language/locallang_db.xml:fields.tt_address_gender.m', 'm'),
							array('LLL:EXT:mr_direct_mail/Resources/Private/Language/locallang_db.xml:fields.tt_address_gender.f', 'f')
						)
					)
				)
			),
			'types' => array(
				'5' => array(
					'showitem' => 'tt_address_groups;LLL:EXT:mr_direct_mail/Resources/Private/Language/locallang_db.xml:fields.tt_address_groups_formlabel, tt_address_gender;LLL:EXT:mr_direct_mail/Resources/Private/Language/locallang_db.xml:fields.tt_address_gender_formlabel'
				)
			)
		);
	}
}
