<?php

if (!defined('TYPO3_MODE')) {
	die ('Access denied.');
}

/**************************************************************/


/**
 * TCA configuration for the table "tt_address"
 */

if (version_compare(TYPO3_branch, '6.1', '<')) {
	t3lib_div::loadTCA('tt_address');
}

$GLOBALS['TCA']['tt_address']['columns']['module_sys_dmail_html']['config']['default'] = true;

/**************************************************************/

/**
 * User function
 */

if (TYPO3_MODE === 'BE') {
	// Registers the user function in the backend.
	$GLOBALS['TYPO3_CONF_VARS']['EXTCONF']['direct_mail_userfunc']['userFunc'][$_EXTKEY] = array(
		'label' => 'LLL:EXT:mr_direct_mail/Resources/Private/Language/locallang_db.xml:userFunc.tt_address_group_list',
		'class' => 'Tx_MrDirectMail_UserFunction_TtAddressGroupList',
		'method' => 'ttAddressGroupList'
	);
}
