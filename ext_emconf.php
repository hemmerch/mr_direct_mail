<?php

/***************************************************************
 * Extension Manager/Repository config file for ext "mr_direct_mail".
 *
 * Auto generated 26-11-2013 08:44
 *
 * Manual updates:
 * Only the data in the array - everything else is removed by next
 * writing. "version" and "dependencies" must not be touched!
 ***************************************************************/

$EM_CONF[$_EXTKEY] = array(
	'title' => 'Hemmer Direct Mail',
	'description' => 'Adds a user function to select recipients from the table "tt_address_group".',
	'category' => 'module',
	'constraints' => array(
		'depends' => array(
			'typo3' => '4.5.0-6.1.99',
			'direct_mail_userfunc' => '1.4.0-0.0.0'
		)
	),
	'state' => 'stable',
	'author' => 'Eric Chavaillaz',
	'author_email' => 'eric@hemmer.ch',
	'author_company' => 'hemmer.ch SA',
	'version' => '1.0.0',
	'_md5_values_when_last_written' => 'a:7:{s:16:"ext_autoload.php";s:4:"e79a";s:12:"ext_icon.gif";s:4:"aaca";s:14:"ext_tables.php";s:4:"2024";s:43:"Classes/UserFunction/TtAddressGroupList.php";s:4:"29f3";s:46:"Resources/Private/Language/fr.locallang_db.xlf";s:4:"be6a";s:43:"Resources/Private/Language/locallang_db.xlf";s:4:"44a5";s:43:"Resources/Private/Language/locallang_db.xml";s:4:"0861";}'
);
