<?php

$classMap = array();
if (version_compare(TYPO3_branch, '6.0', '<')) {
	$classMap = array(
		'tx_mrdirectmail_userfunction_ttaddressgrouplist' => t3lib_extMgm::extPath('mr_direct_mail') . 'Classes/UserFunction/TtAddressGroupList.php'
	);
}
return $classMap;
